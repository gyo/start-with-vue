# 00 Introduction

## Web を作る技術

- HTML
- CSS
- JavaScript

Web のフロントエンドとは、ユーザーが操作する部分のことで、主に HTML, CSS, JavaScript で作られています。

HTML で文字を書き、CSS で色や形を作り、JavaScript で機能を作っていきます。

## Web サイトを作るためのサービス

[CodeSandbox](https://codesandbox.io/) は、手軽に Web サイトを作れるサービスです。

すぐに Vue.js を使えるテンプレート [Vue Template - CodeSandbox](https://codesandbox.io/s/vue) もあり、手軽に Web サイトを公開できます。

ブラウザで [Vue Template - CodeSandbox](https://codesandbox.io/s/vue) を開き、左カラムの Files > src > App.vue をクリックしてソースコードを表示してみてください。

ソースコードを眺めてみましょう。

`<template>` と `</template>` の間に書かれているものが HTML です。`<script>` と `</script>` の間に書かれているものが JavaScript です。そして、`<style>` と `</style>` の間に書かれているものが CSS です。

## Next

- [01 HTML](01-html.md)
