# 02 CSS

- CSS は色と形を決めるものです
- 名前をつけて、どんな見た目にするか決めることができます
- つけた名前を HTML に 書くと、その見た目が反映されます
- CSS は、`<style>` と `</style>` の間に書きます

## 書き方

次のような形で書きます。CSS では名前の前に `.` を入れるのがルールです。HTML は `class="名前"` のように書くのがルールです。

CSS

```
.名前 {
  色: 白;
  背景: 赤;
  隙間: 一文字分;
}
```

HTML

```vue
<button class="名前">こんにちは</button>
```

## 実際に書いてみる

`color: white;` と書くと文字の色が白くなり、`background: red;` と書くと背景の色が赤くなります。`padding: 1em;` と書くと文字の周りに一文字分の隙間ができます。

`<style>` と `</style>` の間に書かれているものをいったんすべて消して、代わりに次のように書いてください。

CSS

```vue
<style>
.my-button {
  color: white;
  background: red;
  padding: 1em;
}
</style>
```

つづいて、HTML に `class="my-button"` を書き加えてください。ボタンの見た目が変化します。

HTML

```vue
<template>
  <button class="my-button">こんにちは</button>
</template>
```

## よく出てくるもの

### 見出し

見出しは、通常の文字よりもサイズを大きくしたり、線を太くすることが多いです。

HTML

```vue
<template>
  <div>
    <div class="heading">坊っちゃん</div>
    <div>親譲りの無鉄砲で小供の時から損ばかりしている。小学校に居る時分学校の二階から飛び降りて一週間ほど腰を抜かした事がある。</div>
  </div>
</template>
```

CSS

```vue
<style>
.heading {
  font-size: 1.5em;
  font-weight: bold;
}
</style>
```

### ボタン

ボタンは文字の周りを四角く囲い、背景に色をつけることが多いです。

マウスを合わせたときにカーソルの見た目を変えることも多いです。

HTML

```vue
<template>
  <div>
    <div class="button">坊っちゃん</div>
  </div>
</template>
```

CSS

```vue
<style>
.button {
  display: inline-block;
  padding: 1em;
  background: orange;
  color: white;
  font-weight: bold;
  cursor: pointer;
}
</style>
```

ここでは div タグをボタンの見た目にしていますが、button タグをボタンの見た目にすることもできます。

ただし、button 要素はもともと特別な見た目をしているので、一度それをリセットしないといけません。

そのため、button 要素を同じ見た目にするには、次のように書くことになります。

HTML

```vue
<template>
  <div>
    <button class="button">坊っちゃん</button>
  </div>
</template>
```

CSS

```vue
<style>
.button {
  background: transparent;
  padding: 0;
  border: none;
  appearance: none;
  font-size: medium;

  display: inline-block;
  padding: 1em;
  background: orange;
  color: white;
  font-weight: bold;
  cursor: pointer;
}
</style>
```

### リスト

連続する言葉や画像のグループを表示するものです。

一つ一つの要素の間に区切り線を入れることが多いです。

HTML

```vue
<template>
  <div>
    <div class="list">
      <div class="list__item">坊っちゃん</div>
      <div class="list__item">吾輩は猫である</div>
      <div class="list__item">草枕</div>
      <div class="list__item">こころ</div>
      <div class="list__item">三四郎</div>
    </div>
  </div>
</template>
```

CSS

```vue
<style>
.list {
  border-bottom: 1px solid gray;
}

.list__item {
  border-top: 1px solid gray;
  display: block;
  padding: 1em;
}
</style>
```

## 高度な使い方

「CSS では名前の前に `.` を入れるのがルール」「HTML では `class="名前"` のように書くのがルール」と説明しましたが、実はそれ以外にもいろいろなルールがあります。

それらのルールを使いこなすことで、複雑なレイアウトも表現できるようになります。

また、CSS は色と形だけでなく動き（アニメーション）を表現することもできます。動きをつけることで、より魅力的なサイトを作ることができます。

詳しくは、MDN のチュートリアルやリファレンスを参照してください。

[CSS: Cascading Style Sheets | MDN](https://developer.mozilla.org/en-US/docs/Web/CSS)

## Next

- [03 JavaScript](03-javascript.md)
