# 03 JavaScript

- JavaScript はプログラムです
- 「いつ」「何をするか」を書くことができます
- 「いつ」は HTML に書きます
- 「何をするか」は `<script>` と `</script>` の間に書きます
- アラートウィンドウを出したりできます
- 画面の文字を変えたりできます

## 書き方

次のような形で書きます。「いつ」の前には `@` を入れるのがルールです。「何をするか」は `methods: {}` の中に書くのがルールです。`methods: {}` の中では、処理の名前の後ろに `()` を入れるのがルールです。

HTML

```
<button class="my-button" @いつ="処理の名前">こんにちは</button>
```

JavaScript

```
export default {
  methods: {
    処理の名前() {
      具体的な処理;
    }
  }
};
```

## 実際に書いてみる

### ボタンを押したときにアラートウィンドウが表示されるようにする

JavaScript では、`alert("こんにちは")` と書くと、「こんにちは」と書かれたアラートウィンドウを表示できます。

`<script>` と `</script>` の間に書かれているものをいったんすべて消して、代わりに次のように書いてください。

JavaScript

```vue
<script>
export default {
  methods: {
    myAlert() {
      alert("こんにちは");
    }
  }
};
</script>
```

つづいて、HTML に `@click="myAlert()"` を書き加えてください。ボタンをクリックするとアラートウィンドウが表示されるようになります。

HTML

```vue
<template>
  <button class="my-button" @click="myAlert()">こんにちは</button>
</template>
```

### ボタンを押したときに画面の文字が変わるようにする

画面に文字を表示するには HTML を書く必要があり、ボタンを押したときに何かをするには JavaScript を書く必要があります。

いまのままでは JavaScript から HTML を書き換えることが出来ないので、まずは JavaScript と HTML を連携させる必要があります。

JavaScript に `data() {}` を書き、{} の間に `return {};` を書きます。そしてその {} の間に、`連携するものの名前: "文字"` という形で HTML と連携させたい文字を書きます。

そのうえで、HTML で `{{}}` の間に連携するものの名前を書くと、文字を表示することができます。

JavaScript

```vue
<script>
export default {
  data() {
    return {
      text: "JavaScript と連携した文字"
    };
  }
};
</script>
```

HTML

```vue
<template>
  <div>
    <div>{{ text }}</div>
  </div>
</template>
```

JavaScript に定義した文字を HTML に表示することができました。

JavaScript と HTML が連携しているので、JavaScript の値を書き換えれば HTML に表示される文字を変更することができます。

JavaScript の文字を書き換えるには、`this.連携するものの名前 = "新しい文字"` のように書きます。

```vue
<script>
export default {
  data() {
    return {
      text: "JavaScript と連携した文字"
    };
  },
  methods: {
    changeText() {
      this.text = "書き換えた文字";
    }
  }
};
</script>
```

最後に、クリックしたときに changeText が実行されるボタンを作りましょう。

HTML に `<button>ボタン</button>` を書き、`@click="changeText()"` を書き加えれば完成です。

```vue
<template>
  <div>
    <div>{{ text }}</div>
    <button class="my-button" @click="changeText()">ボタン</button>
  </div>
</template>
```

ボタンを押すと JavaScript に書いた changeText が実行され、JavaScript に書いた text の値が変わります。その結果、HTML の文字が変わります。
