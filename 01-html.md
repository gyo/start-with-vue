# 01 HTML

- HTML は文字です
- 文字はタグで囲んで表示します
- タグにはいろんな種類があります
- HTML は、`<template>` と `</template>` の間に書きます

## 書き方

次のような形で書きます。

```
<タグ>文字</タグ>
```

タグの中にタグを書くこともできます。

```
<外側のタグ>
  <内側のタグ>文字</内側のタグ>
</外側のタグ>
```

## 実際に書いてみる

`<template>` と `</template>` の間に書かれているものをいったんすべて消して、代わりに `<div>こんにちは</div>` と書いてください。画面に「こんにちは」と表示されます。

```vue
<template>
  <div>こんにちは</div>
</template>
```

`<div>こんにちは</div>` の代わりに `<button>こんにちは</button>` と書くと、ボタンが表示されます。

```vue
<template>
  <button>こんにちは</button>
</template>
```

## よく使うタグ

### div

とくに意味のないタグ。縦に並びます。

```vue
<template>
  <div>
    <div>おはよう</div>
    <div>こんにちは</div>
    <div>こんばんは</div>
  </div>
</template>
```

### span

とくに意味のないタグ。横に並びます。

```vue
<template>
  <div>
    <span>おはよう</span>
    <span>こんにちは</span>
    <span>こんばんは</span>
  </div>
</template>
```

### a

リンクのタグ。クリックするとページ遷移する要素を作成します。

遷移する URL を `href="遷移するURL"` のように指定します。

```vue
<template>
  <div>
    <a href="https://www.google.com/">Google</a>
  </div>
</template>
```

### img

画像を表示するタグ。

表示する画像の URL を `src="画像のURL"` のように指定します。

```vue
<template>
  <div>
    <img src="https://placehold.jp/150x150.png">
  </div>
</template>
```

### button

ボタンのタグ。

見た目がボタンっぽくなり、クリックできるようになります。

```vue
<template>
  <div>
    <button>ボタン</button>
  </div>
</template>
```

### input

文字の入力欄やチェックボックスといった、ユーザーの入力を受けつける要素のタグ。

`type="タイプ"` を変えることで、いろいろな役割をもたせることができます。

```vue
<template>
  <div>
    <div>
      <input type="text">
    </div>
    <div>
      <input type="checkbox">
    </div>
  </div>
</template>
```

## SEO 対策などで必要なタグ

### title

Web ページのタイトルのタグです。

Google Search の検索結果に表示されます。

このタグは `<template>` と `</template>` の間には書きません。Files > src > index.html に書きます。

```vue
<title>Web ページのタイトル</title>
```

### meta

Web ページのメタ情報を表すタグです。

`name="description"` を指定することで、Web ページの概要を書くことができます。

Web ページの概要は Google Search の検索結果に表示されます。

このタグは `<template>` と `</template>` の間には書きません。Files > src > index.html に書きます。

```vue
<meta name="description" content="Web ページの概要です。このページは……"><meta>
```

### h1

見出しのタグ。

```vue
<template>
  <div>
    <h1>見出し</h1>
  </div>
</template>
```

## セマンティクス、アクセシビリティ

HTML のタグには多くの種類があり、それぞれに役割があります。

コンテンツに合わせて適切なタグをつけることで、人と機械の両者にわかりやすい Web サイトを作ることができます。

タグの一覧は、MDN のリファレンスなどを参照してください。

[HTML elements reference - HTML: HyperText Markup Language | MDN](https://developer.mozilla.org/en-US/docs/Web/HTML/Element)

## Next

- [02 CSS](02-css.md)
